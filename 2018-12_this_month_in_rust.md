## Reading List
* [Niko Matsakis: After NLL: Moving from borrowed data and the sentinel pattern](http://smallcultfollowing.com/babysteps/blog/2018/11/10/after-nll-moving-from-borrowed-data-and-the-sentinel-pattern/)
    * You have &mut Foo and you need to move Foo.x in order to replace Foo.x with a new value. What do?
    * std::mem::replace lets you swap Foo.x with a value that already exists (a placeholder value, for instance).
    * Continued discussion on URLO ([link](https://users.rust-lang.org/t/blog-post-series-after-nll-whats-next-for-borrowing-and-lifetimes/21864))
* [Getting started with nightly async/await support](https://jsdw.me/posts/rust-asyncawait-preview/)
* [Converting AsyncRead and AsyncWrite to Futures, Sinks and Streams](https://jsdw.me/posts/rust-futures-tokio/)
* [Bootstrapping My Embedded Rust Development Environment](https://josh.robsonchase.com/embedded-bootstrapping/)
    * Hardware debugger, IDE integration, etc
* [Ralf Jung: Stacked Borrows Implemented](https://www.ralfj.de/blog/2018/11/16/stacked-borrows-implementation.html)
* [David Tolnay: Rust Quiz](https://dtolnay.github.io/rust-quiz/18)
* [Safe Web Services with Actix and Sentry](https://blog.sentry.io/2018/12/04/safe-web-services-actix-sentry?utm_campaign=rust&utm_source=social&utm_medium=twitter&utm_content=post&utm_term=actix)

## Crates
* [Zola (Gutenberg) 0.5.0](https://www.vincentprouillet.com/blog/releasing-zola-0-5-0/)
    * One of the two most popular static site generators written in Rust
    * Renamed from Gutenberg to Zola due to naming conflicts with Wordpress
* A crate for reading/writing to eMMC/SD cards from embedded devices
    * [Reddit discussion](https://reddit.com/r/rust/comments/a07k6e/wip_a_no_std_rust_crate_for_reading_sdmmc_cards/)
    * [Repo](https://github.com/thejpster/embedded-sdmmc-rs)
* A crate for using nVidia's CUDA framework
    * [Reddit discussion](https://www.reddit.com/r/rust/comments/a270py/announcing_rustacuda_v010/)
    * [Blog post](https://bheisler.github.io/post/announcing-rustacuda/)
    * [Repo](https://github.com/bheisler/RustaCUDA)
* [Diffing algorithms](https://crates.io/crates/diffs)
* [Async networking primitives from WithoutBoats](https://github.com/withoutboats/romio)
* Sval: a no-std serialization-only framework
    * [Reddit discussion](https://www.reddit.com/r/rust/comments/a2kn7y/sval_a_prototype_nostd_objectsafe/)
    * [Repo](https://github.com/KodrAus/sval)
* [cargo-review-deps](https://github.com/ferrous-systems/cargo-review-deps)
    * See a diff of dependency updates
* [Jorge Aparicio: cargo-call-stack](https://github.com/japaric/cargo-call-stack)
    * Static analysis of the call stack
    * Geared towards no-std applications
    * Generates a graphviz file for visualization
* MutGuard
    * Execute code when a piece of data is mutably borrowed
    * Written by the author of nom
    * [Reddit discussion](https://www.reddit.com/r/rust/comments/9wcujb/mutguard_run_code_every_time_data_is_mutably/)
    * [Repo](https://github.com/geal/mutguard)
* Pretty progress bars with indicatif
    * [Tweet from Armin Ronacher](https://twitter.com/mitsuhiko/status/1069329798403686400)
    * [Reddit discussion](https://www.reddit.com/r/rust/comments/a2qdgd/a_gif_demo_of_the_newest_release_of_indicatif_a/)
* [cargo-inspect](https://github.com/mre/cargo-inspect)
    * [Blog post](https://matthias-endler.de/2018/cargo-inspect/)
    * See what your code desugars into
* [LSDeluxe (lsd)](https://github.com/Peltoche/lsd)
    * A colorful replacement for ls
* [rsevents](https://github.com/neosmart/rsevents)
    * Multithreading/synchronization primitives similar to Windows events
    * [Blog post](https://neosmart.net/blog/2018/rsevents-manual-and-auto-reset-events-for-rust/)

## Rust in Production
* Amazon Firecracker MicroVMs
    * [Official announcement](https://aws.amazon.com/blogs/opensource/firecracker-open-source-secure-fast-microvm-serverless/)
    * [Project homepage](https://firecracker-microvm.github.io)
* AWS Lambda Runtime for Rust
    * [Reddit discussion](https://reddit.com/r/rust/comments/a1jkof/rust_support_has_been_added_to_aws_lambda/)
    * [Official announcement](https://aws.amazon.com/blogs/opensource/rust-runtime-for-aws-lambda/)
    * Uses serde to (de)serialize events
    * (link to crate)
* [New game studio, Embark, will build games in Rust](https://reddit.com/r/rust/comments/9viryw/embark_a_newly_found_game_studio_will_build_their/)
* [Exonum - Blockchain company shifts from Iron to Actix-Web](https://medium.com/meetbitfury/generic-methods-in-rust-how-exonum-shifted-from-iron-to-actix-web-7a2752171388)
    * "Previously, concerns were raised that actix-web contained a lot of unsafe code. However, the amount of unsafe code was significantly reduced when the framework was rewritten in a safe programming language — Rust."
    * Huh?
* [Safe Web Services with Actix and Sentry](https://blog.sentry.io/2018/12/04/safe-web-services-actix-sentry?utm_campaign=rust&utm_source=social&utm_medium=twitter&utm_content=post&utm_term=actix)

## Core Team and WGs
* Tide (WIP web framework from the networking-wg)
    * [Middleware proposal](https://rust-lang-nursery.github.io/wg-net/2018/11/07/tide-middleware.html)
    * [Evolution of the middleware proposal after feedback](https://rust-lang-nursery.github.io/wg-net/2018/11/27/tide-middleware-evolution.html)
    * Simpler middleware interface, nested routers
* [This year in embedded Rust](https://rust-embedded.github.io/blog/2018-11-14-this-year-in-embedded-rust/)
    * Embedded Rust on stable channel
    * Centralized documentation ([here](https://docs.rust-embedded.org/))
    * Wishlist for 2019 is open [here](https://github.com/rust-embedded/wg/issues/256)
* [This Week in Rust and WebAssembly 009](https://rustwasm.github.io/2018/11/28/this-week-in-rust-wasm-009.html)
* Rust Survey 2018 Results
    * [Results page](https://blog.rust-lang.org/2018/11/27/Rust-survey-2018.html)
    * [Reddit discussion](https://www.reddit.com/r/rust/comments/a0wuac/rust_survey_2018_results/)
    * 5991 responses
    * 51.8% of responders have used Rust for <1 year
    * 59% don't feel productive yet, or took >1 month to get productive
    * No one uses the beta channel
    * 34.4% use Rust at work in some capacity
* [Networking-WG Survey 2018 Results](https://rust-lang-nursery.github.io/wg-net/2018/11/28/wg-net-survey.html)
* [Rust 2018 Beta Release](https://internals.rust-lang.org/t/announcing-rust-2018-beta-release/8901)
* [Verified email required to publish to crates.io starting 2019-02-28](https://users.rust-lang.org/t/a-verified-email-address-will-be-required-to-publish-to-crates-io-starting-on-2019-02-28/22425)
* [TWiR 259](https://this-week-in-rust.org/blog/2018/11/06/this-week-in-rust-259/)
* [TWiR 260](https://this-week-in-rust.org/blog/2018/11/13/this-week-in-rust-260/)
* [TWiR 261](https://this-week-in-rust.org/blog/2018/11/20/this-week-in-rust-261/)
* [TWiR 262](https://this-week-in-rust.org/blog/2018/11/27/this-week-in-rust-262/)
* Stabilize some futures-related stuff
    * [Pull request](https://github.com/rust-lang/rfcs/pull/2592)
    * [Reddit discussion](https://www.reddit.com/r/rust/comments/9vypw3/rfc_stabilize_stdtask_and_stdfuturefuture_by/)
    * Most people don't like the proposed API and don't want it to be stabilized yet
* [More on RLS version numbering](https://www.ncameron.org/blog/more-on-rls-version-numbering/)

## Community
* [Niko Matsakis: After NLL: Moving from borrowed data and the sentinel pattern](http://smallcultfollowing.com/babysteps/blog/2018/11/10/after-nll-moving-from-borrowed-data-and-the-sentinel-pattern/)
    * You have &mut Foo and you need to move Foo.x in order to replace Foo.x with a new value. What do?
    * std::mem::replace lets you swap Foo.x with a value that already exists (a placeholder value, for instance).
    * Continued discussion on URLO ([link](https://users.rust-lang.org/t/blog-post-series-after-nll-whats-next-for-borrowing-and-lifetimes/21864))
* [WithoutBoats: Anchored and Uniform Paths](https://boats.gitlab.io/blog/post/anchored-uniform/)
    * Two different methods for importing/using modules are still on the table for Rust 2018.
    * Seems like the Big Names are leaning towards uniform paths.
* [Serverless HTTP](https://medium.com/@softprops/serverless-http-9a58f9b2df60)
* [Getting started with nightly async/await support](https://jsdw.me/posts/rust-asyncawait-preview/)
* [Converting AsyncRead and AsyncWrite to Futures, Sinks and Streams](https://jsdw.me/posts/rust-futures-tokio/)
* [Bootstrapping My Embedded Rust Development Environment](https://josh.robsonchase.com/embedded-bootstrapping/)
    * Hardware debugger, IDE integration, etc
* [Pre-RFC: Trait privacy](https://phaazon.net/blog/rust-traits-privacy)
* [Niko Matsakis: MIR-based borrowck is almost here](http://smallcultfollowing.com/babysteps/blog/2018/10/31/mir-based-borrowck-is-almost-here/)
* [How to speed up the Rust compiler in 2018: NLL edition](https://blog.mozilla.org/nnethercote/2018/11/06/how-to-speed-up-the-rust-compiler-in-2018-nll-edition/)
    * Lots of tiny improvements, some major ones.
    * A longer read, but a good one
* [Niko Matsakis: After NLL: Interprocedural conflicts](http://smallcultfollowing.com/babysteps/blog/2018/11/01/after-nll-interprocedural-conflicts/)
* [WithoutBoats: Shifgrethor IV: Tracing](https://boats.gitlab.io/blog/post/shifgrethor-iv/)
* [A hammer you can only hold by the handle](https://blog.systems.ethz.ch/blog/2018/a-hammer-you-can-only-hold-by-the-handle.html)
* [On dealing with owning and borrowing in public interfaces](https://phaazon.net/blog/on-owning-borrowing-pub-interface)
* [Things Rust doesn’t let you do](https://medium.com/@GolDDranks/things-rust-doesnt-let-you-do-draft-f596a3c740a5)
* [Truly zero cost](https://vorner.github.io/2018/11/11/truly-zero-cost.html)
* [Monadic do notation in Rust: Part I](https://varkor.github.io/blog/2018/11/10/monadic-do-notation-in-rust-part-i.html)
* [Ralf Jung: Stacked Borrows Implemented](https://www.ralfj.de/blog/2018/11/16/stacked-borrows-implementation.html)
* [Bringing Elm's architecture to Rust and Webassembly](https://sindrejohansen.no/blog/willow/rust/elm/2018/11/16/willow-elm-in-rust.html)
* [Programming Servo: A background-hang-monitor.](https://medium.com/programming-servo/programming-servo-a-background-hang-monitor-73e89185ce1)
* [Program Synthesis is Possible in Rust](http://fitzgeraldnick.com/2018/11/15/program-synthesis-is-possible-in-rust.html)
* Rust Cheat Sheet
    * [Cheat Sheet](https://cheats.rs)
    * [Repo](https://github.com/ralfbiedert/cheats.rs)
* [Rust book available in ePub format](https://www.jyotirmoy.net/posts/2018-12-01-rust-book.html)
* [David Tolnay: Rust Quiz](https://dtolnay.github.io/rust-quiz/18)
* [Build Your Own Shell using Rust](https://www.joshmcguigan.com/blog/build-your-own-shell-rust/)
* [The What and How of Futures and async/await in Rust](https://www.youtube.com/watch?v=9_3krAQtD2k)
    * Screencast, 4 hours long
* [Ian Hobson - An introduction to Rust for audio developers](https://www.youtube.com/watch?v=Yom9E-67bdI)
    * Audio Developers Conference 2018
    * 49 minutes long
* [This month in rustsim #2](https://www.rustsim.org/blog/2018/12/01/this-month-in-rustsim/)
* [wasm-bindgen — how does it work?!](http://fitzgeraldnick.com/2018/12/02/wasm-bindgen-how-does-it-work.html)
* [Creating my first AWS Lambda using Rust](https://medium.com/@kkostov/rust-aws-lambda-30a1b92d4009)
* [New Rustacean E027: Trust Me; I Promise!](https://newrustacean.com/show_notes/e027/)
* [TabNine: Autocompleter written in Rust](https://www.reddit.com/r/rust/comments/9uhc1x/tabnine_an_autocompleter_for_all_languages_built/)
* [Pijul 0.11 Released](https://pijul.org/posts/2018-11-20-pijul-0.11/)
* [How I Wrote a Modern C++ Library in Rust](https://hsivonen.fi/modern-cpp-in-rust/)
* [Patterns of Refactoring C to Rust: The case of librsvg](https://people.gnome.org/~federico/blog/guadec-2018-presentation.html)

## Reddit/URLO
* [The great GitHub color change](https://reddit.com/r/rust/comments/9wyam7/the_old_github_color_is_back/)
* [Cranelift produces faster code than rustc in some situations](https://reddit.com/r/rust/comments/9xobgz/the_cranelift_backend_for_rustc_now_produces_code/)
* [Amethyst Foundation has been formed](https://www.reddit.com/r/gamedev/comments/a01cft/amethyst_foundation_has_been_formed/)
* [jemalloc removed as the default allocator](https://reddit.com/r/rust/comments/9twam5/jemalloc_was_just_removed_from_the_standard/)
* [A bad tag line for embedded Rust](https://reddit.com/r/rust/comments/a1sowj/i_came_up_with_a_catchline_for_rust_embedded_rust/)
* [Using numpy and Rust together](https://users.rust-lang.org/t/pass-numpy-array-to-rust-function-using-pyo3/22804)
* [Actix projects to learn from](https://reddit.com/r/rust/comments/a23cl8/anybody_have_an_actix_project_i_can_learn_from/)
